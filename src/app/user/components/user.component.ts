import {Component, EventEmitter, Output} from "@angular/core";
import {User} from "../../models/users.model"

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['user.component.css']
})
export class UserComponent {
  @Output() loggedIn: EventEmitter<string> = new EventEmitter<string>()

  constructor() {
  }

  public username: string = '';

  public onLogin(): void {
    console.log("login clicked")
    let users: any = localStorage.getItem('users')
    let currentUser: User | null = null

    if (users) {
      users = JSON.parse(users)
      let foundUser = false

      for (let u of users) {
        if(this.username === u.username) {
          currentUser = u;
          foundUser = true;
        }
      }

      if (!foundUser) {
        let newUser: User = {
          username: this.username,
          backpack: []
        }
        users.push(newUser)
        currentUser = newUser
      }

    } else {
      let newUser: User = {
        username: this.username,
        backpack: []
      }
      users = [newUser]
      currentUser = newUser
    }

    localStorage.setItem('users', JSON.stringify(users))
    localStorage.setItem('loggedInUser', JSON.stringify(currentUser))

    this.loggedIn.emit(this.username)
  }
}
