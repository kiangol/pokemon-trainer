import {NgModule} from "@angular/core";
import {UserPage} from "./pages/user.page";
import {FormsModule} from "@angular/forms";
import {UserComponent} from "./components/user.component";
import {UserRoutingModule} from "./user-routing.module";

@NgModule({
  declarations: [
    UserPage,
    UserComponent
  ],
  imports: [
    FormsModule,
    UserRoutingModule
  ]
})

export class UserModule {}
