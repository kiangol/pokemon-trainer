import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-page',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.css']
})

export class UserPage {
  constructor(private readonly router: Router) {
  }

  loginSuccess() {
    return this.router.navigate(['catalog'])
  }
}
