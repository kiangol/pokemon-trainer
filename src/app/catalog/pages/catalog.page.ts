import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-catalog-page',
  templateUrl: './catalog.page.html',
  styleUrls: ['catalog.page.css']
})

export class CatalogPage {
  constructor(private readonly router: Router) {
    let loggedInUser = localStorage.getItem('loggedInUser')
    if(!loggedInUser) {
      this.router.navigate(['login'])
    }
  }

  goToTrainer() {
    console.log("Trainer")
    this.router.navigate(['trainer'])
  }

  signOut() {
    localStorage.removeItem('currentUser')
    this.router.navigate(['login'])
  }
}
