import {Component, Input, OnInit} from "@angular/core";
import {PokemonsService} from "../../../services/pokemons.service";
import {Pokemon} from "../../../models/pokemon.model";

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {
  @Input() pokemon: Pokemon | undefined;

  constructor(private readonly pokemonsService: PokemonsService) {
  }

  ngOnInit(): void {
  }

  public select(): void {
    console.log("Selected! " + this.pokemon?.id)
  }
}
