import {Component, OnInit} from "@angular/core";
import {PokemonsService} from "../../../services/pokemons.service";
import {Pokemon} from "../../../models/pokemon.model";

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit{
  constructor(private readonly pokemonsService: PokemonsService) {
  }

  ngOnInit(): void {
    this.pokemonsService.fetchPokemons()
  }

  get pokemons(): Pokemon[] {
    return this.pokemonsService.pokemons()
  }

  onClick(pokemon: Pokemon) {
    let user = localStorage.getItem('loggedInUser')
    if(!user) {
      return
    }
    let loggedInUser = JSON.parse(user)
    let inBackpack = false

  }

  public next(): void {
    window.scroll(0,0);
    return this.pokemonsService.next()
  }
  public previous(): void {
    window.scroll(0,0);
    return this.pokemonsService.previous()
  }
}
