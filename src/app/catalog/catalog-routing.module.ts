import {RouterModule, Routes} from "@angular/router";
import {CatalogPage} from "./pages/catalog.page";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: '',
    component: CatalogPage
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class CatalogRoutingModule {}
