import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Pokemon} from "../../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {
  private offset: number = 0
  private _amount: number = 10;
  private _pokemonUrls = []
  private _pokemons: Pokemon[] = [];
  private _error: string = '';
  private _baseUrl: string = 'https://pokeapi.co/api/v2/pokemon?limit=10';
  private _nextUrl: string = ''
  private _previousUrl: string = ''

  constructor(private readonly http: HttpClient) {
  }

  public fetchPokemons(url: string = this._baseUrl): void {
    this.http.get<Pokemon[]>(url)
      .subscribe((pokemons) => {
        this._pokemonUrls = JSON.parse(JSON.stringify(pokemons)).results;

        for (let i = 0; i < this._pokemonUrls.length; i++) {
          this._pokemons[i] = this._pokemonUrls[i]
          this._pokemons[i].id = String(i + 1 + this.offset)
        }

        this._nextUrl = JSON.parse(JSON.stringify(pokemons)).next;
        this._previousUrl = JSON.parse(JSON.stringify(pokemons)).previous;
        console.log(this._pokemonUrls)
      }, (error: HttpErrorResponse) => {
        this._error = error.message
      });
  }

  public next(): void {
    if (this._nextUrl) {
      this.fetchPokemons(this._nextUrl);
      this.offset += this._amount;
    }
  }

  public previous(): void {
    if (this._previousUrl) {
      this.fetchPokemons(this._previousUrl);
      this.offset -= this._amount;
    }
  }

  public pokemons(): Pokemon[] {
    return this._pokemons;
  }

  public error(): string {
    return this._error;
  }
}
