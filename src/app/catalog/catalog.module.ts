import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CatalogPage} from "./pages/catalog.page";
import {PokemonListComponent} from "./components/pokemon-list/pokemon-list.component";
import {PokemonComponent} from "./components/pokemon/pokemon.component";
import {CatalogRoutingModule} from "./catalog-routing.module";

@NgModule({
  declarations: [
    CatalogPage,
    PokemonListComponent,
    PokemonComponent
  ],
  imports: [
    CatalogRoutingModule,
    CommonModule
  ]
})
export class CatalogModule {}
