import {Component, Input} from "@angular/core";
import {Pokemon} from "../../models/pokemon.model";

@Component({
  selector: 'app-trainer-pokemon',
  templateUrl: 'trainer-pokemon.component.html',
  styleUrls: ['trainer-pokemon.component.css']
})

export class TrainerPokemonComponent {
  @Input() pokemon: Pokemon | undefined;
}
