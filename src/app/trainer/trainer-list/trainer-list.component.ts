import {Component} from "@angular/core";
import {Pokemon} from "../../models/pokemon.model"

@Component({
  selector: 'app-trainer-list',
  templateUrl: 'trainer-list.component.html',
  styleUrls: ['trainer-list.component.css']
})

export class TrainerListComponent {
  constructor() {}

  get pokemon$(): Pokemon[] {
    let currentUser = localStorage.getItem('currentUser')
    if (currentUser != null) {
      let userObj = JSON.parse(currentUser)
      return userObj.backpack
    }
    return []
  }
}
