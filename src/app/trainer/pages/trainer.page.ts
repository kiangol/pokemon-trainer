import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-trainer-page',
  templateUrl: 'trainer.page.html',
  styleUrls: ['trainer.page.css']
})

export class TrainerPage {
  constructor (private readonly router: Router) {
    let loggedInUser = localStorage.getItem('loggedInUser')
    if (!loggedInUser) {
      this.router.navigate(['login'])
    }
  }
}
