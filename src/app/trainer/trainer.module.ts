import {NgModule} from "@angular/core";
import {TrainerPage} from "./pages/trainer.page";
import {CommonModule} from "@angular/common";
import {TrainerListComponent} from "./trainer-list/trainer-list.component";
import {TrainerPokemonComponent} from "./trainer-pokemon/trainer-pokemon.component";
import {TrainerRoutingModule} from "./trainer-routing.module";


@NgModule({
  declarations: [
    TrainerPage,
    TrainerListComponent,
    TrainerPokemonComponent
  ],
  imports: [
    TrainerRoutingModule,
    CommonModule
  ]
})

export class TrainerModule {}
